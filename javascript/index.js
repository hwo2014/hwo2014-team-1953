var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var speedArray = []

var __ = require('underscore')._;

var prePiece = {}

var preSpeedDown = null;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

function consoleLogPiece(pieceId, type, data) {  
  console.log(data)
}

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {    
    if (data.data) {
      var botData = __.find(data.data, function(car) {return car.id.name==botName})      
      consoleLogPiece(botData.piecePosition.pieceIndex,"data",speedArray[botData.piecePosition.pieceIndex])

      var pieceSpeed = speedArray[botData.piecePosition.pieceIndex].speed

      if (Math.abs(botData.angle) > 5) {
        consoleLogPiece(botData.piecePosition.pieceIndex,"fast","ToFast! "+botData.angle)
        var speedDown = preSpeedDown || Math.floor(Math.abs(botData.angle)/10) / 10        
        pieceSpeed = pieceSpeed - speedDown        
      } else {
        speedDown = null
      }

      consoleLogPiece(botData.piecePosition.pieceIndex,"sum","Piece "+botData.piecePosition.pieceIndex+" set speed "+pieceSpeed)

      send({
        msgType: "throttle",      
        data: pieceSpeed
      });
      /*send({
        msgType: "throttle",      
        data: 0.65
      });*/
    }
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'crash') {
      console.log('CRASH !!!');
    } else if (data.msgType === 'dnf') {
      console.log('DISQUALIFIED !!!');
    } else if (data.msgType === 'gameInit') {
      // get track information
      var track = data.data.race.track      
      __.each(track.pieces, function(piece, i) {        
        if (piece.radius == undefined) {
          speedArray[i] = {speed: 1, info: piece, id: i}
        } else if (piece.switch) {
          speedArray[i] = {speed: 0.8, info: piece, id: i}
        } else if (piece.angle) {
          if (Math.abs(piece.angle/piece.radius) > 0.3) {
            speedArray[i] = {speed: 0.5, info: piece, id: i}
          } else {
            speedArray[i] = {speed: 0.65, info: piece, id: i}
          }

        } else {
          speedArray[i] = {speed: 0.65, info: piece, id: i}
        }
      })      
      __.each(speedArray, function(piece, i) {        
        if (speedArray[i+1] && Math.abs(piece.speed - speedArray[i+1].speed) > 0.1) {
          piece.speed = (piece.speed + speedArray[i+1].speed) / 2          
        }
      })
      __.each(speedArray, function(piece, i) {        
        if (speedArray[i+1] && Math.abs(piece.speed - speedArray[i+1].speed) > 0.1) {
          piece.speed = (piece.speed + speedArray[i+1].speed) / 2          
        }
      })
      __.each(speedArray, function(piece, i) {        
        if (speedArray[i+1] && Math.abs(piece.speed - speedArray[i+1].speed) > 0.1) {
          piece.speed = (piece.speed + speedArray[i+1].speed) / 2          
        }
      })      
      console.log(speedArray)
    }

    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});
